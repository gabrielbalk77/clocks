package clock;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 * A view of a clock that looks like a typical clock with hands.
 * 
 * @author Chuck Cusack, 2001. Extensive refactoring, January 2013.
 */
public class AnalogClock extends ClockView {
	public static final int		width		= 450;
	public static final int		height		= 450;
	public static final int		xCenter		= 225;
	public static final int		yCenter		= 225;
	public static final Font	FACE_FONT	= new Font(Font.SANS_SERIF, Font.BOLD, 24);

	public AnalogClock(ClockInterface clock) {
		super(width, height, clock);
	}

	/*
	 * Abstract methods from ClockView.
	 */
	@Override
	public void drawFace(Graphics g) {
		g.setColor(Color.black);
		g.fillOval(0, 0, width, height);
		g.setColor(new Color(100, 200, 100));
		g.fillOval(35, 35, width - 70, height - 70);
		// Draw the numbers on the face
		g.setFont(FACE_FONT);
		for (int i = 1; i < 13; ++i) {
			double Agl = i / 12.0 * 2 * Math.PI + (3 * Math.PI / 2);
			g.drawString(i + "", xCenter - 10 + (int) (Math.cos(Agl) * (width / 2 - 15)),
					yCenter + 10 + (int) (Math.sin(Agl) * (height / 2 - 20)));
		}
	}

	@Override
	public void drawSeconds(Graphics g) {
		double SecondAgl = 2 * Math.PI * (getSeconds() / 60.0) + (3 * Math.PI / 2);
		g.setColor(Color.red);
		g.drawLine(xCenter, yCenter, xCenter + (int) (Math.cos(SecondAgl) * 190), yCenter
				+ (int) (Math.sin(SecondAgl) * 190));
	}

	@Override
	public void drawMinutes(Graphics g) {
		double MinuteAgl = 2 * Math.PI * (getMinutes() / 60.0) + (3 * Math.PI / 2) + 2 * Math.PI
				* (getSeconds() / (60.0 * 60));
		g.setColor(Color.blue);
		g.drawLine(xCenter, yCenter, xCenter + (int) (Math.cos(MinuteAgl) * 150), yCenter
				+ (int) (Math.sin(MinuteAgl) * 150));
	}

	@Override
	public void drawHours(Graphics g) {
		double HourAgl = 2 * Math.PI * (getHours() / 12.0) + (3 * Math.PI / 2) + 2 * Math.PI
				* (getMinutes() / (60.0 * 12)) + +2 * Math.PI * (getSeconds() / (60.0 * 60 * 12));
		g.setColor(Color.orange);
		g.drawLine(xCenter, yCenter, xCenter + (int) (Math.cos(HourAgl) * 100), yCenter
				+ (int) (Math.sin(HourAgl) * 100));
	}
}
